package random;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.HttpStatus;

import java.util.UUID;

@Controller("//")
public class RandomController {

    @Get("/")
    public String random() {
        return UUID.randomUUID().toString();
    }
}